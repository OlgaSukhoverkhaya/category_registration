USE [CategoryRegistrationDB]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 05.11.2018 15:57:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SectorCategories]    Script Date: 05.11.2018 15:57:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SectorCategories](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_SectorCategories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sectors]    Script Date: 05.11.2018 15:57:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sectors](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SectorName] [nvarchar](200) NOT NULL,
	[SectorCategoryId] [bigint] NOT NULL,
 CONSTRAINT [PK_Sectors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubSectors]    Script Date: 05.11.2018 15:57:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubSectors](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SubSectorName] [nvarchar](200) NOT NULL,
	[SectorId] [bigint] NOT NULL,
 CONSTRAINT [PK_SubSectors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 05.11.2018 15:57:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](200) NOT NULL,
	[AgreeToTerms] [bit] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserSectors]    Script Date: 05.11.2018 15:57:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserSectors](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[SubSectorId] [bigint] NULL,
	[SectorId] [bigint] NOT NULL,
 CONSTRAINT [PK_UserSectors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181030134347_CreateDB', N'2.1.3-rtm-32065')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181101144240_AlterUserSector', N'2.1.3-rtm-32065')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181101144400_AlterUserSector2', N'2.1.3-rtm-32065')
SET IDENTITY_INSERT [dbo].[SectorCategories] ON 

INSERT [dbo].[SectorCategories] ([Id], [CategoryName]) VALUES (20, N'Manufacturing')
INSERT [dbo].[SectorCategories] ([Id], [CategoryName]) VALUES (21, N'Other')
INSERT [dbo].[SectorCategories] ([Id], [CategoryName]) VALUES (22, N'Service')
SET IDENTITY_INSERT [dbo].[SectorCategories] OFF
SET IDENTITY_INSERT [dbo].[Sectors] ON 

INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10104, N'Construction materials', 20)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10105, N'Tourism', 22)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10106, N'Information Technology and Telecommunications', 22)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10107, N'Engineering', 22)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10108, N'Business services', 22)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10109, N'Environment', 21)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10110, N'Energy technology', 21)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10111, N'Creative industries', 21)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10112, N'Translation services', 22)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10113, N'Wood', 20)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10114, N'Printing ', 20)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10115, N'Plastic and Rubber', 20)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10116, N'Metalworking', 20)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10117, N'Machinery', 20)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10118, N'Furniture', 20)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10119, N'Food and Beverage', 20)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10120, N'Electronics and Optics', 20)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10121, N'Textile and Clothing', 20)
INSERT [dbo].[Sectors] ([Id], [SectorName], [SectorCategoryId]) VALUES (10122, N'Transport and Logistics', 22)
SET IDENTITY_INSERT [dbo].[Sectors] OFF
SET IDENTITY_INSERT [dbo].[SubSectors] ON 

INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10293, N'Bakery and confectionery products', 10119)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10294, N'CNC-machining', 10116)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10295, N'Forgings, Fasteners ', 10116)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10296, N'Gas, Plasma, Laser cutting', 10116)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10297, N'MIG, TIG, Aluminum welding', 10116)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10298, N'Packaging', 10115)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10299, N'Plastic goods', 10115)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10300, N'Plastic processing technology', 10115)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10301, N'Blowing', 10115)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10302, N'Moulding', 10115)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10303, N'Plastics welding and processing', 10115)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10304, N'Plastic profiles', 10115)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10305, N'Advertising', 10114)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10306, N'Book/Periodicals printing', 10114)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10307, N'Labelling and packaging printing', 10114)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10308, N'Clothing', 10121)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10309, N'Textile', 10121)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10310, N'Other (Wood)', 10113)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10311, N'Wooden building materials', 10113)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10312, N'Wooden houses', 10113)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10313, N'Data processing, Web portals, E-marketing', 10106)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10314, N'Programming, Consultancy', 10106)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10315, N'Software, Hardware', 10106)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10316, N'Telecommunications', 10106)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10317, N'Air', 10122)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10318, N'Rail', 10122)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10319, N'Metal works', 10116)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10320, N'Road', 10122)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10321, N'Metal products', 10116)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10322, N'Construction of metal structures', 10116)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10323, N'Beverages', 10119)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10324, N'Fish and fish products ', 10119)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10325, N'Meat and meat products', 10119)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10326, N'Milk and dairy products ', 10119)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10327, N'Other', 10119)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10328, N'Sweets and snack food', 10119)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10329, N'Bathroom/sauna ', 10118)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10330, N'Bedroom', 10118)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10331, N'Children’s room ', 10118)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10332, N'Kitchen ', 10118)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10333, N'Living room ', 10118)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10334, N'Office', 10118)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10335, N'Other (Furniture)', 10118)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10336, N'Outdoor ', 10118)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10337, N'Project furniture', 10118)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10338, N'Machinery components', 10117)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10339, N'Machinery equipment/tools', 10117)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10340, N'Manufacture of machinery ', 10117)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10341, N'Maritime', 10117)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10342, N'Aluminium and steel workboats ', 10117)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10343, N'Boat/Yacht building', 10117)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10344, N'Ship repair and conversion', 10117)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10345, N'Metal structures', 10117)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10346, N'Other', 10117)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10347, N'Repair and maintenance service', 10117)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10348, N'Houses and buildings', 10116)
INSERT [dbo].[SubSectors] ([Id], [SubSectorName], [SectorId]) VALUES (10349, N'Water', 10122)
SET IDENTITY_INSERT [dbo].[SubSectors] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [UserName], [AgreeToTerms]) VALUES (111, N'Anton Makarov', 1)
INSERT [dbo].[Users] ([Id], [UserName], [AgreeToTerms]) VALUES (112, N'Tamara Antonova', 1)
INSERT [dbo].[Users] ([Id], [UserName], [AgreeToTerms]) VALUES (113, N'Tamara Bulkina', 1)
SET IDENTITY_INSERT [dbo].[Users] OFF
SET IDENTITY_INSERT [dbo].[UserSectors] ON 

INSERT [dbo].[UserSectors] ([Id], [UserId], [SubSectorId], [SectorId]) VALUES (307, 111, 10293, 10119)
INSERT [dbo].[UserSectors] ([Id], [UserId], [SubSectorId], [SectorId]) VALUES (308, 111, NULL, 10104)
INSERT [dbo].[UserSectors] ([Id], [UserId], [SubSectorId], [SectorId]) VALUES (309, 111, NULL, 10120)
INSERT [dbo].[UserSectors] ([Id], [UserId], [SubSectorId], [SectorId]) VALUES (314, 112, 10345, 10117)
INSERT [dbo].[UserSectors] ([Id], [UserId], [SubSectorId], [SectorId]) VALUES (317, 113, 10293, 10119)
INSERT [dbo].[UserSectors] ([Id], [UserId], [SubSectorId], [SectorId]) VALUES (318, 113, NULL, 10120)
SET IDENTITY_INSERT [dbo].[UserSectors] OFF
ALTER TABLE [dbo].[UserSectors] ADD  DEFAULT (CONVERT([bigint],(0))) FOR [SectorId]
GO
ALTER TABLE [dbo].[Sectors]  WITH CHECK ADD  CONSTRAINT [FK_Sectors_SectorCategories_SectorCategoryId] FOREIGN KEY([SectorCategoryId])
REFERENCES [dbo].[SectorCategories] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sectors] CHECK CONSTRAINT [FK_Sectors_SectorCategories_SectorCategoryId]
GO
ALTER TABLE [dbo].[SubSectors]  WITH CHECK ADD  CONSTRAINT [FK_SubSectors_Sectors_SectorId] FOREIGN KEY([SectorId])
REFERENCES [dbo].[Sectors] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SubSectors] CHECK CONSTRAINT [FK_SubSectors_Sectors_SectorId]
GO
ALTER TABLE [dbo].[UserSectors]  WITH CHECK ADD  CONSTRAINT [FK_UserSectors_Sectors_SectorId] FOREIGN KEY([SectorId])
REFERENCES [dbo].[Sectors] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserSectors] CHECK CONSTRAINT [FK_UserSectors_Sectors_SectorId]
GO
ALTER TABLE [dbo].[UserSectors]  WITH CHECK ADD  CONSTRAINT [FK_UserSectors_SubSectors_SubSectorId] FOREIGN KEY([SubSectorId])
REFERENCES [dbo].[SubSectors] ([Id])
GO
ALTER TABLE [dbo].[UserSectors] CHECK CONSTRAINT [FK_UserSectors_SubSectors_SubSectorId]
GO
ALTER TABLE [dbo].[UserSectors]  WITH CHECK ADD  CONSTRAINT [FK_UserSectors_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserSectors] CHECK CONSTRAINT [FK_UserSectors_Users_UserId]
GO
