﻿using CategoryRegistration.Core.Models;
using System.Threading.Tasks;

namespace CategoryRegistration.Repository.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task AddUserAsync(User user);
        Task UpdateUserAsync(User user);
        User GetUserByUserName(string userName);
    }
}
