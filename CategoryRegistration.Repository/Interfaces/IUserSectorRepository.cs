﻿using CategoryRegistration.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CategoryRegistration.Repository.Interfaces
{
    public interface IUserSectorRepository
    {
        Task AddRangeUserSectorsAsync(IList<UserSector> userSectors);
        Task RemoveUserSectorsAsync(User user);
    }
}
