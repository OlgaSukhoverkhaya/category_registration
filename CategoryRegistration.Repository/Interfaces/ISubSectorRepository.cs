﻿using CategoryRegistration.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CategoryRegistration.Repository.Interfaces
{
    public interface ISubSectorRepository : IRepository<SubSector>
    {
        Task<SubSector> GetSubSectorByIdAsync(long id);
        Task AddSubSectorsAsync(List<SubSector> subSectors);
    }
}
