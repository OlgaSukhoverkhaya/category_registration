﻿using CategoryRegistration.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CategoryRegistration.Repository.Interfaces
{
    public interface ISectorCategoryRepository : IRepository<SectorCategory>
    {
        Task<IList<SectorCategory>> GetAllCategoriesIncludeSectorsAndSubSectorsAsync();
        Task AddSectorCategoriesAsync(List<SectorCategory> sectorCategories);
    }
}
