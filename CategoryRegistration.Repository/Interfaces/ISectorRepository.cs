﻿using CategoryRegistration.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CategoryRegistration.Repository.Interfaces
{
    public interface ISectorRepository : IRepository<Sector>
    {
        Task<Sector> GetSectorByIdAsync(long id);
        Task AddSectorsAsync(List<Sector> sectors);
        Task <IList<Sector>> GetAllSectorsAsync();
    }
}
