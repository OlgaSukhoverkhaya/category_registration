﻿using CategoryRegistration.Core.Models;
using CategoryRegistration.DataAcess.DataAcess;
using CategoryRegistration.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CategoryRegistration.Repository.Implementation
{
    public class SubSectorRepository :ISubSectorRepository
    {
        private readonly ApplicationContext _context;
        private DbSet<SubSector> _entities;

        public SubSectorRepository(ApplicationContext context)
        {
            this._context = context;
            _entities = context.Set<SubSector>();
        }

        public async Task<SubSector> GetSubSectorByIdAsync(long id)
        {
            var SubSector = await _entities
                .Include(item => item.Sector)
                .FirstOrDefaultAsync(s => s.Id == id);

            return SubSector;
        }

        public async Task AddSubSectorsAsync(List<SubSector> subSectors)
        {
            await _entities.AddRangeAsync(subSectors);
            await _context.SaveChangesAsync();
        }
    }
}
