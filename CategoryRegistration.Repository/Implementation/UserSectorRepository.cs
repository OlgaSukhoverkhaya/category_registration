﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CategoryRegistration.Core.Models;
using CategoryRegistration.DataAcess.DataAcess;
using CategoryRegistration.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CategoryRegistration.Repository.Implementation
{
    public class UserSectorRepository : IUserSectorRepository
    {
        private readonly ApplicationContext _context;
        private DbSet<UserSector> _entities;

        public UserSectorRepository(ApplicationContext context)
        {
            this._context = context;
            this._entities = context.Set<UserSector>();
        }


        public async Task AddRangeUserSectorsAsync(IList<UserSector> userSectors)
        {
            await _entities.AddRangeAsync(userSectors);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveUserSectorsAsync(User user)
        {
            var userSectors = _entities.Where(item => item.User.Id == user.Id);
            _entities.RemoveRange(userSectors);
            await _context.SaveChangesAsync();
        }
    }
}
