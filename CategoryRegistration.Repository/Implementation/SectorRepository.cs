﻿using CategoryRegistration.Core.Models;
using CategoryRegistration.DataAcess.DataAcess;
using CategoryRegistration.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CategoryRegistration.Repository.Implementation
{
    public class SectorRepository : ISectorRepository
    {
        private readonly ApplicationContext context;
        private DbSet<Sector> _entities;

        public SectorRepository(ApplicationContext context)
        {
            this.context = context;
            _entities = context.Set<Sector>();
        }

        public async Task AddSectorsAsync(List<Sector> sectors)
        {
            await _entities.AddRangeAsync(sectors);
            await context.SaveChangesAsync();
        }

        public async Task<Sector> GetSectorByIdAsync(long id)
        {
            var Sector = await _entities
                .FirstOrDefaultAsync(s => s.Id == id);

            return Sector;
        }

        public async Task<IList<Sector>> GetAllSectorsAsync()
        {
            return await _entities.ToListAsync();
        }
    }
}
