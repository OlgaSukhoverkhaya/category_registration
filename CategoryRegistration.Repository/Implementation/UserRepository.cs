﻿using CategoryRegistration.Core.Models;
using CategoryRegistration.DataAcess.DataAcess;
using CategoryRegistration.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CategoryRegistration.Repository.Implementation
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationContext _context;
        private DbSet<User> _entities;

        public UserRepository(ApplicationContext context)
        {
            this._context = context;
            _entities = context.Set<User>();
        }

        public async Task AddUserAsync(User user)
        {
            if (GetUserByUserName(user.UserName) == null)
            {
                await _entities.AddAsync(user);
                await _context.SaveChangesAsync();
            }
            else throw new InvalidOperationException(user.UserName + " username alreasy exists!");
        }

        public async Task UpdateUserAsync(User user)
        {
            if (GetUserByUserName(user.UserName) == null)
            {
                _entities.Update(user);
                await _context.SaveChangesAsync();
            }
            else throw new InvalidOperationException(user.UserName + " username alreasy exists!");
        }

        public User GetUserByUserName(string userName)
        {
            var user = _entities.FirstOrDefault(item => item.UserName == userName);
            return user ?? null;
        }
    }
}
