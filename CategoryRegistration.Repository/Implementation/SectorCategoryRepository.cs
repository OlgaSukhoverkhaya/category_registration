﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CategoryRegistration.Core.Models;
using CategoryRegistration.DataAcess.DataAcess;
using CategoryRegistration.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CategoryRegistration.Repository.Implementation
{
    public class SectorCategoryRepository : ISectorCategoryRepository
    {
        private readonly ApplicationContext _context;
        private DbSet<SectorCategory> _entities;

        public SectorCategoryRepository(ApplicationContext context)
        {
            this._context = context;
            _entities = context.Set<SectorCategory>();
        }

        public async Task AddSectorCategoriesAsync(List<SectorCategory> sectorCategories)
        {
            await _entities.AddRangeAsync(sectorCategories);
            await _context.SaveChangesAsync();
        }

        public async Task<IList<SectorCategory>> GetAllCategoriesIncludeSectorsAndSubSectorsAsync()
        {
            List<SectorCategory> sectorsCategories = await _entities
                 .Include(category => category.Sectors)
                 .ThenInclude(sector => sector.SubSectors)
                 .ToListAsync();

            return sectorsCategories;
        }
    }
}
