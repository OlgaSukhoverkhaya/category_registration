﻿using CategoryRegistration.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace CategoryRegistration.DataAcess.DataAcess
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Sector> Sectors { get; set; }
        public DbSet<UserSector> UserSectors { get; set; }
        public DbSet<SubSector> SubSectors { get; set; }
        public DbSet<SectorCategory> SectorCategories { get; set; }
    }
}
