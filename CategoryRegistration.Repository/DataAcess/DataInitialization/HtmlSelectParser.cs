﻿using CategoryRegistration.Core.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CategoryRegistration.Repository.DataAcess.DataInitialization
{
    public static class HtmlSelectParser
    {
        public static List<SectorCategory> sectorCategories { get; private set; } = new List<SectorCategory>();
        public static List<Sector> sectors { get; private set; } = new List<Sector>();
        public static List<SubSector> subSectors { get; private set; } = new List<SubSector>();

        private static HtmlDocument LoadDocument(string path)
        {
            HtmlDocument hap = new HtmlDocument();
            hap.Load(path);
            return hap;
        }

        private static List<string> GetOptionsList(HtmlDocument hap)
        {
            HtmlNode select = hap.DocumentNode.SelectSingleNode("//select");
            List<string> allOptions = new List<string>();
            if (select != null)
            {
                allOptions =
                    select.SelectNodes("option")
                    .Select(option => option.InnerText).ToList();
            }
            return allOptions.Count != 0 ? allOptions : throw new Exception("Collection is empty!");
        }

        public static void FillData(string path)
        {
           HtmlDocument hap = LoadDocument(path);
            var allOptions = GetOptionsList(hap);

            foreach (var item in allOptions)
            {
                if (!item.Contains("&nbsp;"))
                {
                    var category = new SectorCategory { CategoryName = item };
                    sectorCategories.Add(category);
                }
                else
                {
                    var itemWithoutAmp = item.Replace("&amp;", "and");
                    if (Regex.Matches(itemWithoutAmp, "&nbsp;").Count < 5)
                    {
                        var sectorName = itemWithoutAmp.Replace("&nbsp;", "");
                        var sector = new Sector() { SectorName = sectorName, SectorCategory = sectorCategories.Last() };
                        sectors.Add(sector);
                    }
                    if (Regex.Matches(itemWithoutAmp, "&nbsp;").Count > 4)
                    {
                        var subSectorName = itemWithoutAmp.Replace("&nbsp;", "");
                        subSectors.Add(new SubSector() { SubSectorName = subSectorName, Sector = sectors.Last() });
                    }
                }
            }
        }
    }
}
