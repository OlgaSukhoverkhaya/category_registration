﻿using CategoryRegistration.Core.Models;
using CategoryRegistration.DataAcess.DataAcess;
using CategoryRegistration.Repository.DataAcess.DataInitialization;
using CategoryRegistration.Repository.Interfaces;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CategoryRegistration
{
    public class InitializationData
    {
        private const string _path = @"..\CategoryRegistration.Repository\DataAcess\files\index.html";

        private ISectorRepository _sectorRepository;
        private ISubSectorRepository _subSectorRepository;
        private ISectorCategoryRepository _sectorCategoryRepository;

        public InitializationData(ISectorRepository sectorRepo,
            ISubSectorRepository subSectorRepo,
            ISectorCategoryRepository sectorCategoryRepo)
        {
            this._sectorCategoryRepository = sectorCategoryRepo;
            this._subSectorRepository = subSectorRepo;
            this._sectorRepository = sectorRepo;
        }

        public async Task InitializeSectorsAsync()
        {
            var sectors = await _sectorRepository.GetAllSectorsAsync();
            if (sectors.Count == 0)
            {
                HtmlSelectParser.FillData(_path);
                await _sectorCategoryRepository.AddSectorCategoriesAsync(HtmlSelectParser.sectorCategories);
                await _sectorRepository.AddSectorsAsync(HtmlSelectParser.sectors);
                await _subSectorRepository.AddSubSectorsAsync(HtmlSelectParser.subSectors);
            }
        }
    }
}
