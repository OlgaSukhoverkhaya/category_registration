﻿//fields validation
$('#registrationForm').validate({
    rules: {
        'model.SectorIds':
        {
            required: true,
            minlength: 1
        }
    },
    errorClass: "error-block",
    errorPlacement: function (error, element) {
        if (element.attr("id") == "name"
            || element.attr("id") == "terms-agree") {
            error.insertAfter(element);
        }
        else {
            error.insertAfter("#select");
        }
    },
    messages: {
        'model.SectorIds':
        {
            required: "Please check at least one sector!"
        }
    }
});

//auto checkbox selection
$("input.checkbox-child")
    .change(function () {
        if ($(this).attr('checked','checked')) {
            var follower = $(this).parent()
                .prevAll('li.group')
                .find('input.checkbox-group')
                .last();
            follower.attr('checked', 'checked');
        }
    });
