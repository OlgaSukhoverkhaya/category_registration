﻿using CategoryRegistration.DataAcess;
using CategoryRegistration.DataAcess.DataAcess;
using CategoryRegistration.Repository.Implementation;
using CategoryRegistration.Repository.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CategoryRegistration//TODO do i need all of those services?
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            //context added as a sevice
            string connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApplicationContext>(
                options => options.UseSqlServer(connection,
                m => m.MigrationsAssembly("CategoryRegistration")));

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddTransient<ISectorCategoryRepository, SectorCategoryRepository>();
            services.AddTransient<ISubSectorRepository, SubSectorRepository>();
            services.AddTransient<ISectorRepository, SectorRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IUserSectorRepository, UserSectorRepository>();
            services.AddTransient<InitializationData>();
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, InitializationData initializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            initializer.InitializeSectorsAsync().Wait();
        }
    }
}
