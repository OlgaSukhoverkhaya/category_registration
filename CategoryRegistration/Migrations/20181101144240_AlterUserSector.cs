﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CategoryRegistration.Migrations
{
    public partial class AlterUserSector : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sectors_SectorCategories_SectorCategoryId",
                table: "Sectors");

            migrationBuilder.DropForeignKey(
                name: "FK_SubSectors_Sectors_SectorId",
                table: "SubSectors");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSectors_Sectors_SectorId",
                table: "UserSectors");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSectors_Users_UserId",
                table: "UserSectors");

            migrationBuilder.DropIndex(
                name: "IX_UserSectors_SectorId",
                table: "UserSectors");

            migrationBuilder.DropColumn(
                name: "SectorId",
                table: "UserSectors");

            migrationBuilder.AlterColumn<long>(
                name: "UserId",
                table: "UserSectors",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "SubSectorId",
                table: "UserSectors",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AlterColumn<string>(
                name: "UserName",
                table: "Users",
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SubSectorName",
                table: "SubSectors",
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "SectorId",
                table: "SubSectors",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SectorName",
                table: "Sectors",
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "SectorCategoryId",
                table: "Sectors",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CategoryName",
                table: "SectorCategories",
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserSectors_SubSectorId",
                table: "UserSectors",
                column: "SubSectorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Sectors_SectorCategories_SectorCategoryId",
                table: "Sectors",
                column: "SectorCategoryId",
                principalTable: "SectorCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubSectors_Sectors_SectorId",
                table: "SubSectors",
                column: "SectorId",
                principalTable: "Sectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSectors_SubSectors_SubSectorId",
                table: "UserSectors",
                column: "SubSectorId",
                principalTable: "SubSectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSectors_Users_UserId",
                table: "UserSectors",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sectors_SectorCategories_SectorCategoryId",
                table: "Sectors");

            migrationBuilder.DropForeignKey(
                name: "FK_SubSectors_Sectors_SectorId",
                table: "SubSectors");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSectors_SubSectors_SubSectorId",
                table: "UserSectors");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSectors_Users_UserId",
                table: "UserSectors");

            migrationBuilder.DropIndex(
                name: "IX_UserSectors_SubSectorId",
                table: "UserSectors");

            migrationBuilder.DropColumn(
                name: "SubSectorId",
                table: "UserSectors");

            migrationBuilder.AlterColumn<long>(
                name: "UserId",
                table: "UserSectors",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<long>(
                name: "SectorId",
                table: "UserSectors",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserName",
                table: "Users",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200);

            migrationBuilder.AlterColumn<string>(
                name: "SubSectorName",
                table: "SubSectors",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200);

            migrationBuilder.AlterColumn<long>(
                name: "SectorId",
                table: "SubSectors",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<string>(
                name: "SectorName",
                table: "Sectors",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200);

            migrationBuilder.AlterColumn<long>(
                name: "SectorCategoryId",
                table: "Sectors",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<string>(
                name: "CategoryName",
                table: "SectorCategories",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200);

            migrationBuilder.CreateIndex(
                name: "IX_UserSectors_SectorId",
                table: "UserSectors",
                column: "SectorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Sectors_SectorCategories_SectorCategoryId",
                table: "Sectors",
                column: "SectorCategoryId",
                principalTable: "SectorCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubSectors_Sectors_SectorId",
                table: "SubSectors",
                column: "SectorId",
                principalTable: "Sectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSectors_Sectors_SectorId",
                table: "UserSectors",
                column: "SectorId",
                principalTable: "Sectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSectors_Users_UserId",
                table: "UserSectors",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
