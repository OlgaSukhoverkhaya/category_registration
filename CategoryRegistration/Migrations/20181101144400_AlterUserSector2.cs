﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CategoryRegistration.Migrations
{
    public partial class AlterUserSector2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserSectors_SubSectors_SubSectorId",
                table: "UserSectors");

            migrationBuilder.AlterColumn<long>(
                name: "SubSectorId",
                table: "UserSectors",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<long>(
                name: "SectorId",
                table: "UserSectors",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_UserSectors_SectorId",
                table: "UserSectors",
                column: "SectorId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserSectors_Sectors_SectorId",
                table: "UserSectors",
                column: "SectorId",
                principalTable: "Sectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSectors_SubSectors_SubSectorId",
                table: "UserSectors",
                column: "SubSectorId",
                principalTable: "SubSectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserSectors_Sectors_SectorId",
                table: "UserSectors");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSectors_SubSectors_SubSectorId",
                table: "UserSectors");

            migrationBuilder.DropIndex(
                name: "IX_UserSectors_SectorId",
                table: "UserSectors");

            migrationBuilder.DropColumn(
                name: "SectorId",
                table: "UserSectors");

            migrationBuilder.AlterColumn<long>(
                name: "SubSectorId",
                table: "UserSectors",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSectors_SubSectors_SubSectorId",
                table: "UserSectors",
                column: "SubSectorId",
                principalTable: "SubSectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
