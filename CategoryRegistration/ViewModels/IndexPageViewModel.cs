﻿using CategoryRegistration.Core.Models;
using System.Collections.Generic;

namespace CategoryRegistration.ViewModels
{
    public class IndexPageViewModel
    {
        public User User { get; set; } = new User();
        public List<SectorCategory> SectorsByCategory { get; set; } = new List<SectorCategory>();
        public List<long> SectorIds { get; set; } = new List<long>();
        public List<long> SubSectorIds { get; set; } = new List<long>();
        public string Result { get; set; }
    }
}
