﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using CategoryRegistration.ViewModels;
using CategoryRegistration.Repository.Interfaces;
using CategoryRegistration.Core.Models;
using System.Collections.Generic;
using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CategoryRegistration.Controllers
{
    public class HomeController : Controller
    {
        private const string UserKey = "User";
        private ISectorCategoryRepository _sectorCategoryRepository;
        private ISubSectorRepository _subSectorRepository;
        private ISectorRepository _sectorRepository;
        private IUserRepository _userRepository;
        private IUserSectorRepository _userSectorRepository;

        public HomeController(ISectorCategoryRepository sectorCategoryRepository,
            ISubSectorRepository subSectorRepository,
            ISectorRepository sectorRepository,
            IUserRepository userRepository,
            IUserSectorRepository userSectorRepository)
        {
            this._sectorCategoryRepository = sectorCategoryRepository;
            this._subSectorRepository = subSectorRepository;
            this._sectorRepository = sectorRepository;
            this._userRepository = userRepository;
            this._userSectorRepository = userSectorRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await GetSectorsBySectorCategoryAsync());
        }

        [HttpPost]
        public async Task<IActionResult> SaveAsync(IndexPageViewModel model)
        {
            if (GetCurrentUserFromSession() != null)
            {
                model = await UpdateDataAsync(model);
            }
            else
            {
                model = await SaveDataAsync(model);
            }

            SaveCurrentUserToSession(model.User);
            var userSectors = await ComposeUserSectorListAsync(model);
            await _userSectorRepository.AddRangeUserSectorsAsync(userSectors);

            model.SectorsByCategory = await ComposeSavedUserSectorListAsync(model);
            return View("Index", model);
        }

        private async Task<IndexPageViewModel> SaveDataAsync(IndexPageViewModel model)
        {
            string message = "Yor data is saved successfully!";
            try
            {
                await _userRepository.AddUserAsync(model.User);
            }
            catch (Exception ex)
            {
                message = "Data is not saved! error: " + ex.Message;
            }
            finally
            {
                model.Result = message;
            }
            return model;
        }

        private async Task<IndexPageViewModel> UpdateDataAsync(IndexPageViewModel model)
        {
            string message = "Yor data is updated successfully!";
            try
            {
                model.User.Id = GetCurrentUserFromSession().Id;
                await _userSectorRepository.RemoveUserSectorsAsync(model.User);

                if (model.User.UserName != GetCurrentUserFromSession().UserName)
                {
                    await _userRepository.UpdateUserAsync(model.User);
                }
            }
            catch (Exception ex)
            {
                message = "Data is not updated! error: " + ex.Message;
            }
            finally
            {
                model.Result = message;
            }
            return model;
        }

        private void SaveCurrentUserToSession(User user)
        {
            try
            {
                var currentUser = _userRepository.GetUserByUserName(user.UserName);
                HttpContext.Session.SetString(UserKey,
                    JsonConvert.SerializeObject(currentUser));
            }
            catch (Exception) { }
        }

        private User GetCurrentUserFromSession()
        {
            var user = HttpContext.Session.GetString(UserKey);
            return user == null ? null : JsonConvert.DeserializeObject<User>(user);
        }

        private async Task<List<SectorCategory>> ComposeSavedUserSectorListAsync(IndexPageViewModel model)
        {
            var sectorsByCategories = await GetSectorsBySectorCategoryAsync();
            sectorsByCategories.SectorsByCategory
                .ForEach(cat => cat.Sectors.ToList()
                .ForEach(sec =>
                    {
                        sec.IsChecked =
                        model.SectorIds.Contains(sec.Id) ? true : false;
                        if (sec.SubSectors != null)
                            sec.SubSectors.ToList().ForEach(sub =>
                            {
                                sub.IsChecked =
                                  model.SubSectorIds.Contains(sub.Id) ? true : false;
                                if (sub.IsChecked && sub.Sector == sec && !sec.IsChecked) sec.IsChecked = true;
                            });
                    }));

            return sectorsByCategories.SectorsByCategory;
        }

        private async Task<List<UserSector>> ComposeUserSectorListAsync(IndexPageViewModel model)
        {
            List<UserSector> userSectors = new List<UserSector>();
            //var ids= model.SectorIds;
            if (model.SubSectorIds != null && model.SubSectorIds.Count > 0)
            {
                userSectors.AddRange(await UserSectorListFromSubSectorIdsAsync(model));
                model.SectorIds = await RemoveExcessSectorIds(model);
            }

            if (model.SectorIds != null && model.SectorIds.Count() > 0)
            {
                userSectors.AddRange(await UserSectorListFromSectorIdsAsync(model));
            }
            //model.SectorIds = ids;
            return userSectors;
        }

        private async Task<List<UserSector>> UserSectorListFromSubSectorIdsAsync(IndexPageViewModel model)
        {
            var userSectors = new List<UserSector>();

            foreach (var subSectorId in model.SubSectorIds)
            {
                var concreteSubSector =
                    await this._subSectorRepository.GetSubSectorByIdAsync(subSectorId);

                userSectors.Add(
                    new UserSector
                    {
                        User = model.User,
                        SubSector = concreteSubSector,
                        Sector = concreteSubSector.Sector
                    });
            }
            return userSectors;
        }

        private async Task<List<long>> RemoveExcessSectorIds(IndexPageViewModel model)
        {
            foreach (var id in model.SubSectorIds)
            {
                var subSector = await _subSectorRepository
                    .GetSubSectorByIdAsync(id);

                if (model.SectorIds.Contains(subSector.Sector.Id))
                {
                    model.SectorIds =
                        model.SectorIds
                        .Where(val => val != subSector.Sector.Id).ToList();
                }
            }
            return model.SectorIds;
        }

        private async Task<List<UserSector>> UserSectorListFromSectorIdsAsync(IndexPageViewModel model)
        {
            var userSectors = new List<UserSector>();

            foreach (var sectorId in model.SectorIds)
            {
                userSectors.Add(
                    new UserSector
                    {
                        User = model.User,
                        Sector = await _sectorRepository.GetSectorByIdAsync(sectorId)
                    });
            }

            return userSectors;
        }

        private async Task<IndexPageViewModel> GetSectorsBySectorCategoryAsync()
        {
            var sectorsCategories =
                await this._sectorCategoryRepository
                .GetAllCategoriesIncludeSectorsAndSubSectorsAsync();
            return new IndexPageViewModel() { SectorsByCategory = sectorsCategories.ToList() };
        }
    }
}
