﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CategoryRegistration.Core.Models
{
    public class SubSector : BaseModel
    {
        [Required]
        [MaxLength(200)]
        public string SubSectorName { get; set; }
        [Required]
        public Sector Sector { get; set; }

        [NotMapped]
        public bool IsChecked { get; set; } = false;
    }
}
