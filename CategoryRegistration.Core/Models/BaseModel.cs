﻿namespace CategoryRegistration.Core.Models
{
    public abstract class BaseModel
    {
        public long Id { get; set; }
    }
}
