﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CategoryRegistration.Core.Models
{
    public class Sector : BaseModel
    {
        [Required]
        [MaxLength(200)]
        public string SectorName { get; set; }
        [Required]
        public SectorCategory SectorCategory { get; set; }

        [JsonIgnore]
        public ICollection<SubSector> SubSectors { get; set; }

        [NotMapped]
        public bool IsChecked { get; set; } = false;
    }
}
