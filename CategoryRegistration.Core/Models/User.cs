﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CategoryRegistration.Core.Models
{
    public class User : BaseModel
    {
        [Required]
        [MaxLength(200)]
        public string UserName { get; set; }
        public bool AgreeToTerms { get; set; }

        [JsonIgnore]
        public ICollection<UserSector> userSectors { get; set; }
    }
}
