﻿using System.ComponentModel.DataAnnotations;

namespace CategoryRegistration.Core.Models
{
    public class UserSector : BaseModel
    {
        [Required]
        public User User { get; set; }
        [Required]
        public Sector Sector { get; set; }
        public SubSector SubSector { get; set; }

    }
}
