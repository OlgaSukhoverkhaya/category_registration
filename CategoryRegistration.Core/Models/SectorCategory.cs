﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CategoryRegistration.Core.Models
{
    public class SectorCategory : BaseModel
    {
        [Required]
        [MaxLength(200)]
        public string CategoryName { get; set; }
        [Required]
        public IList<Sector> Sectors { get; set; }
    }
}
